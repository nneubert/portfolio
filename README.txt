Included in this portfolio is a selection of python projects by Nathaniel Neubert.

Each folder contains a jupyter notebook or simple python program to be reviewed by potential employers.

LoveEquations is an exploration of ODEs
MarkovChains explores the use of Markov Chains in natural language processing
NMF_Recommender examines the use of Non-Negative Matrix Factorization to generate recommendations
PageRank Orders the value of items based on the flow of an adjacency matrix
PDE Solution includes the solution to a PDE for a final in a course that I was very proud of
RandomForest includes a Random Forest Classifier with accuracy comparable to sklearn
SVD_ImageCompression although done early on in my python career, is important to me because of
	my introduction to principal component analysis
WeightLoss is another example of solving ODE's
facial_recognition is a jupyter notebook of a method for doing facial recognition without using sklearn

Cancer_Modeling.pdf describes an ODE model of cancer growth with treatment and treatment resistance it is 
	accompanied by the corresponding Jupyter notebook Cancer_Modeling.ipynb
Cell_Function_Identification describes efforts to identify key genes in the liver using machine learning
	techniques

In addition, I include the school assignments which I used to learn the basics of Pandas.

This repository is only to be used of the purpose of assessing the Nathaniel Neubert's skills in programming
	for hiring purposes. Any questions should be sent to nathanielneubert@gmail.com