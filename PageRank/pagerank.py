# solutions.py
"""Volume 1: The Page Rank Algorithm.
Nathaniel Neubert
Volume 1
3/3/2020
"""

import numpy as np
import networkx as nx

# Problems 1-2
class DiGraph:
    """A class for representing directed graphs via their adjacency matrices.

    Attributes:
        (fill this out after completing DiGraph.__init__().)
    """

    def __init__(self, A, labels=None):
        """Modify A so that there are no sinks in the corresponding graph,
        then calculate Ahat. Save Ahat and the labels as attributes.

        Parameters:
            A ((n,n) ndarray): the adjacency matrix of a directed graph.
                A[i,j] is the weight of the edge from node j to node i.
            labels (list(str)): labels for the n nodes in the graph.
                If None, defaults to [0, 1, ..., n-1].
        """
        m = A.shape[0] #Find the shape of the array
        
        if labels == None:
            self.labels = []
            for k in range(m):
                self.labels.append(k) # Defaults to [0, 1, ..., n-1]
        else:
            if len(labels) != m:
                 raise ValueError("Number of labels not equal to number of nodes")
            self.labels = labels #Store the labels as desired
        
        self.A = A.copy().astype(float) #Create a duplicate of the inputed matrix but use floats!
        zeros = np.zeros(m)
        for column in range(m):
            if np.sum(self.A[0:,column]) < 1e-5: #Fix sinks
                self.A[0:,column] = 1
            value = np.sum(self.A[0:,column])
            for row in range(m): #Adjust the final values
                if self.A[row,column] > 0:
                    self.A[row,column] = self.A[row,column]/value
            

    def linsolve(self, epsilon=0.85):
        """Compute the PageRank vector using the linear system method.

        Parameters:
            epsilon (float): the damping factor, between 0 and 1.

        Returns:
            dict(str -> float): A dictionary mapping labels to PageRank values.
        """
        m = self.A.shape[0] #get the shape
        I_eA = np.identity(m) - epsilon * self.A #Get the values to solve for
        ones = (1-epsilon)/m * np.ones(m)
        steady = np.linalg.solve(I_eA, ones) #Solve for the steady state
        #steady = steady/np.sum(steady)
        PRvalues = {}
        for k in range(m): #return the steady state dictionary
            PRvalues[self.labels[k]] = steady[k]
        return PRvalues


    def eigensolve(self, epsilon=0.85):
        """Compute the PageRank vector using the eigenvalue method.
        Normalize the resulting eigenvector so its entries sum to 1.

        Parameters:
            epsilon (float): the damping factor, between 0 and 1.

        Return:
            dict(str -> float): A dictionary mapping labels to PageRank values.
        """
        m = self.A.shape[0] #Get the shape
        B = epsilon * self.A + (1-epsilon)/m*np.ones((m,m)) #Find the B matrix
        vals, vecs = np.linalg.eig(B) #Find the eigenvalues
        steady = vecs[0:,0] #select the steady state
        steady = np.real(steady/np.sum(steady))
        PRvalues = {}
        for k in range(m): #return the steady state dictionary
            PRvalues[self.labels[k]] = steady[k]
        return PRvalues
        
    def itersolve(self, epsilon=0.85, maxiter=100, tol=1e-12):
        """Compute the PageRank vector using the iterative method.

        Parameters:
            epsilon (float): the damping factor, between 0 and 1.
            maxiter (int): the maximum number of iterations to compute.
            tol (float): the convergence tolerance.

        Return:
            dict(str -> float): A dictionary mapping labels to PageRank values.
        """
        m = self.A.shape[0] #Record the shape of A
        p0 = np.ones(m)/m #Generate p0
        ones = np.copy(p0)
        
        p1 = epsilon * self.A @ p0 + (1-epsilon)/m #complete one iteration before entering the loop for initial comparison
        PRvalues = {}
        for k in range(maxiter): #iterate until max iter is reached
            if np.linalg.norm(p1-p0,ord=1)<tol: #or we are within our designated tolerance
                p1 = p1/np.sum(p1)
                for j in range(m): #Build and return the proper dictionary
                    PRvalues[self.labels[j]] = p1[j]
                return PRvalues
            p0 = p1
            p1 = epsilon * self.A @ p0 + (1-epsilon)/m #Complete another iteration
            p1 = p1/np.sum(p1)
            
        p1 = p1/np.sum(p1)
        for j in range(m): #Build and return the proper dictionary
            PRvalues[self.labels[j]] = p1[j]
        return PRvalues
        

def get_ranks(d):
    """Construct a sorted list of labels based on the PageRank vector.

    Parameters:
        d (dict(str -> float)): a dictionary mapping labels to PageRank values.

    Returns:
        (list) the keys of d, sorted by PageRank value from greatest to least.
    """
    L = list(d.values()) #Create a list of the dictionary values
    K = list(d.keys()) #Create a list of the dictionary keys
    if type(L[0]) is not int:
        rsort = [x for _, x in sorted(zip(L,K))] #Generates the desired list but in reverse order
        rsort.reverse() #Fix the order of the list
    else:
        rsort = [x for _, x in sorted(zip(L,K))] #Generates the desired list but in reverse order
        rsort.reverse() #Fix the order of the list
        Ksort = K.sort(reverse=True)
        k = 0
        while k < (len(Ksort)):
            j = k
            while Ksort[k] == Ksort[k+1]:
                k += 1
            if j != k:
                sortgroup = rsort[j:k]
                rsort[j:k] = sortgroup.sort(reverse=True)
    return rsort #Return a list of the sorted keys


def rank_websites(filename="web_stanford.txt", epsilon=0.85):
    """Read the specified file and construct a graph where node j points to
    node i if webpage j has a hyperlink to webpage i. Use the DiGraph class
    and its itersolve() method to compute the PageRank values of the webpages,
    then rank them with get_ranks(). If two webpages have the same rank,
    resolve ties by listing the webpage with the larger ID number first.

    Each line of the file has the format
        a/b/c/d/e/f...
    meaning the webpage with ID 'a' has hyperlinks to the webpages with IDs
    'b', 'c', 'd', and so on.

    Parameters:
        filename (str): the file to read from.
        epsilon (float): the damping factor, between 0 and 1.

    Returns:
        (list(str)): The ranked list of webpage IDs.
    """
    data = open(filename, 'r')
    content = data.readlines()
    
    #This block of code creates a list of labels
    labels = []
    index = 0
    for line in content:
        cons = line.split('/')
        cons[-1] = cons[-1][0:len(cons[-1])-1]
        for link in cons:
            if int(link) not in labels:
                labels.append(int(link))
                index += 1
    
    #This block of code creates a dictionary which matches each page to an index of the matrix A
    pages = {}
    labels.sort(reverse=True)
    index = 0
    for link in labels:
        pages[link] = index
        index += 1
        
    #Format the Adjacency matrix A
    A = np.zeros((index,index))
    for line in content:
        cons = line.split('/')
        cons[-1] = cons[-1][0:len(cons[-1])-1]
        for link in cons[1:]:
            A[pages[int(link)],pages[int(cons[0])]] = 1
    
    #Run the algorithm
    tess = DiGraph(A, labels)
    d = tess.itersolve(epsilon)
    ranked = get_ranks(d)
    sites = []
    for rank in ranked:
        sites.append(str(rank))
    return sites
            

def rank_ncaa_teams(filename, epsilon=0.85):
    """Read the specified file and construct a graph where node j points to
    node i with weight w if team j was defeated by team i in w games. Use the
    DiGraph class and its itersolve() method to compute the PageRank values of
    the teams, then rank them with get_ranks().

    Each line of the file has the format
        A,B
    meaning team A defeated team B.

    Parameters:
        filename (str): the name of the data file to read.
        epsilon (float): the damping factor, between 0 and 1.

    Returns:
        (list(str)): The ranked list of team names.
    """
    f = open(filename, 'r')
    games = f.readlines()
    
    #Produce a list of all teams
    teams = []
    for game in games[1:]:
        team = game.split(',')
        if team[0] not in teams:
            teams.append(team[0])
        if team[1][0:-1] not in teams:
            teams.append(team[1][0:-1])
            #print(team[1][0:-1])
    
    teams.sort()
    
    #This block of code creates a dictionary which matches each team to an index of A
    td = {}
    index = 0
    for team in teams:
        td[team] = index
        index += 1
    #print(type(td))
    
    # Format the adjacency matrix for the teams
    m = len(teams)
    A = np.zeros((m,m))
    for game in games[1:]:
        result = game.split(',')
        A[td[result[0]],td[result[1][0:-1]]] += 1
    #print(np.sum(A, axis=1))
    #print(A)
    
    #Run the algorithm
    tess = DiGraph(A, teams)
    d = tess.itersolve(epsilon)
    return get_ranks(d)
    


def rank_actors(filename="top250movies.txt", epsilon=0.85):
    """Read the specified file and construct a graph where node a points to
    node b with weight w if actor a and actor b were in w movies together but
    actor b was listed first. Use NetworkX to compute the PageRank values of
    the actors, then rank them with get_ranks().

    Each line of the file has the format
        title/actor1/actor2/actor3/...
    meaning actor2 and actor3 should each have an edge pointing to actor1,
    and actor3 should have an edge pointing to actor2.
    """
    f = open(filename, 'r', encoding="utf-8") #Open the file
    lines = f.readlines() #Read the lines which are movies with actors following
    MA = nx.DiGraph() #Initialize the graph
    
    #Loop for each movie
    for movie in lines:
        actors = movie.split('/') #split by actors
        m = len(actors)
        actors[m-1] = actors[m-1][0:-1]
        for k in range(1,m-1):
            for j in range(k+1,m):
                if MA.has_edge(actors[j], actors[k]):
                    MA[actors[j]][actors[k]]["weight"] +=1
                else:
                    MA.add_edge(actors[j], actors[k], weight=1)
    d = nx.pagerank(MA, alpha=epsilon)
    
    return get_ranks(d)
