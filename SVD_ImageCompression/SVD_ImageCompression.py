# solutions.py
"""Volume 1: The SVD and Image Compression. Solutions File."""
from scipy import linalg as la
import numpy as np
from matplotlib import pyplot as plt
from imageio import imread
from numpy import linalg

# Problem 1
def compact_svd(A, tol=1e-6):
    """Compute the truncated SVD of A.

    Parameters:
        A ((m,n) ndarray): The matrix (of rank r) to factor.
        tol (float): The tolerance for excluding singular values.

    Returns:
        ((m,r) ndarray): The orthonormal matrix U in the SVD.
        ((r,) ndarray): The singular values of A as a 1-D array.
        ((r,n) ndarray): The orthonormal matrix V^H in the SVD.
    """
    #Get the Eigenvalues and the EigenVectors
    EigenVals, Vectors = la.eig(A.conj().T @ A)
    SingularVals = np.sqrt(EigenVals)
    
    # Get the singular values and sort them
    indices = np.argsort(SingularVals)[::-1]
    SV1 = SingularVals[indices]
    V1 = Vectors[:,indices]
    
    '''for s, v in sorted(zip(SingularVals, Vectors), reverse=True):
        SV1.append(s)
        V1.append(v)'''
    r = sum(SingularVals > tol)
    SV1 = SV1[:r]
    V1 = V1[:,:r]
    
    # Calculate U
    #print(A)
    #print(V1)
    #print(SV1)
    U = A @ V1/SV1
    SV = np.diag(SV1)
    #Return the SVD
    return U,SV1,V1.conj().T

def Test1():
    #Test of first problem
    A = np.random.random((20,20))
    U, F, VH = compact_svd(A)
    print(U, F, VH)
    if np.allclose(A, U @ np.diag(F) @ VH):
        return True
    return False
    
# Problem 2
def visualize_svd(A):
    """Plot the effect of the SVD of A as a sequence of linear transformations
    on the unit circle and the two standard basis vectors.
    """
    Circle = np.linspace(0,2*np.pi, 200)
    U, F, VH = compact_svd(A)
    F = np.diag(F)
    
    # The Circle Graph
    x = np.cos(Circle)
    y = np.sin(Circle)
    xy = np.vstack((x,y))
    #print(np.shape(xy))
    VHxy = VH @ xy
    #print(np.shape(VHxy))
    FVHxy = F @ VHxy
    #print(np.shape(FVHxy))
    UFVHxy = U @ FVHxy
    #print(np.shape(UFVHxy))
    
    # The xLine and yLine
    xLine = np.array([[0,1],[0,0]])
    yLine = np.array([[0,0],[0,1]])
    #print(np.shape(VH))
    #print(np.shape(x))
    VHx = VH @ xLine
    VHy = VH @ yLine
    FVHx = F @ VHx
    FVHy = F @ VHy
    UFVHx = U @ FVHx
    UFVHy = U @ FVHy
    
    #Original unit Circle
    Original = plt.subplot(221)
    Original.plot(xy[0],xy[1])
    Original.plot(xLine[0],xLine[1])
    Original.plot(yLine[0],yLine[1])
    plt.axis("equal") 
    
    #VH transformation
    Part1 = plt.subplot(222)
    Part1.plot(VHxy[0], VHxy[1])
    Part1.plot(VHx[0],VHx[1])
    Part1.plot(VHy[0],VHy[1])
    plt.axis("equal") 
    
    Part2 = plt.subplot(223)
    Part2.plot(FVHxy[0], FVHxy[1])
    Part2.plot(FVHx[0],FVHx[1])
    Part2.plot(FVHy[0],FVHy[1])
    plt.axis("equal") 
    
    Part3 = plt.subplot(224)
    Part3.plot(UFVHxy[0], UFVHxy[1])
    Part3.plot(UFVHx[0],UFVHx[1])
    Part3.plot(UFVHy[0],UFVHy[1])
    plt.axis("equal") 
    plt.show()

# Problem 3
def svd_approx(A, s):
    """Return the best rank s approximation to A with respect to the 2-norm
    and the Frobenius norm, along with the number of bytes needed to store
    the approximation via the truncated SVD.

    Parameters:
        A ((m,n), ndarray)
        s (int): The rank of the desired approximation.

    Returns:
        ((m,n), ndarray) The best rank s approximation of A.
        (int) The number of entries needed to store the truncated SVD.
    """
    #Generate the normal SVD of A
    U, F, VH = compact_svd(A)
    m,n = np.shape(A)
    #print(np.size(A))
    #Raise value error if s is larger than the rank of A
    if s > np.size(F):
        raise ValueError("s input is larger than the rank of A")
    return U[0:m,0:s],F[0:s],VH[0:s,0:m], np.size(U[:,0:s])+np.size(F[0:s])+np.size(VH[:,0:s])


# Problem 4
def lowest_rank_approx(A, err):
    """Return the lowest rank approximation of A with error less than 'err'
    with respect to the matrix 2-norm, along with the number of bytes needed
    to store the approximation via the truncated SVD.

    Parameters:
        A ((m, n) ndarray)
        err (float): Desired maximum error.

    Returns:
        A_s ((m,n) ndarray) The lowest rank approximation of A satisfying
            ||A - A_s||_2 < err.
        (int) The number of entries needed to store the truncated SVD.
    """
    U, F, VH = compact_svd(A)
    m,n = np.shape(A)
    s = 0 #s is the rank of the reduced matrix
    for entry in F: #increase the rank until the matrix is correctly compressed
        if entry > err:
            s = s + 1
    if s == F.size: #If the matrix is not actually compressed
        raise ValueError("the epsilon value is too small to reduce A")
    return U[0:m,0:s],F[0:s],VH[0:s,0:m], np.size(U[:,0:s])+np.size(F[0:s])+np.size(VH[:,0:s])


# Problem 5
def compress_image(filename, s):
    """Plot the original image found at 'filename' and the rank s approximation
    of the image found at 'filename.' State in the figure title the difference
    in the number of entries used to store the original image and the
    approximation.

    Parameters:
        filename (str): Image file path.
        s (int): Rank of new image.
    """
    #Load the image file
    image = imread(filename) / 255
    Osize = np.size(image)
    #Determine if the image is color
    original = np.copy(image)
    if len(image.shape) == 3:
        color = True
    else:
        color = False
    #In the case of a grayscale image
    if color == False:
        rank = linalg.matrix_rank(image)
        U, F, VH, i = svd_approx(image, s)
        redsize = np.size(U)+np.size(F)+np.size(VH)
        imagecompress = U @ np.diag(F) @ VH
        plt.subplot(121)
        plt.imshow(original, cmap="gray")
        plt.title(str("Orignal Size: " + str(Osize)))
        plt.axis("off")
        plt.subplot(122)
        plt.imshow(np.real(imagecompress), cmap="gray")
        plt.title(str("Compressed Image: " + str(redsize) + ", " + str(Osize -redsize) + " entries saved!"))
        plt.axis("off")
    #In the case of a color image
    if color == True:
        redsize = 0
        rank = linalg.matrix_rank(image[:,:,0])
        U, F, VH, i = svd_approx(image[:,:,0], s) #red
        red = U @ np.diag(F) @ VH
        redsize = np.size(U)+np.size(F)+np.size(VH) + redsize
        U, F, VH, i = svd_approx(image[:,:,1], s) #green
        green = U @ np.diag(F) @ VH
        redsize = np.size(U)+np.size(F)+np.size(VH) + redsize
        U, F, VH, i = svd_approx(image[:,:,2], s) #blue
        blue = U @ np.diag(F) @ VH
        redsize = np.size(U)+np.size(F)+np.size(VH) + redsize
        image[:,:,0] = red
        image[:,:,1] = green
        image[:,:,2] = blue
        plt.subplot(121)
        plt.imshow(original)
        plt.title(str("Orignal size: " + str(Osize)))
        plt.axis("off")
        plt.subplot(122)
        plt.imshow(np.real(image))
        plt.title(str("Compressed Image: " + str(redsize) + ", " + str(Osize -redsize) + " entries saved!"))
        plt.axis("off")
    plt.show()
    return None
