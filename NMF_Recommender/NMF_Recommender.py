import numpy as np
import pandas as pd
from sklearn.decomposition import NMF
from sklearn.metrics import mean_squared_error as mse


class NMFRecommender:

    def __init__(self,random_state = 15, tol=1e-3, maxiter=200,rank=3):
        """The parameter values for the algorithm"""
        #Save the random state, the tolerance, the maxiter and the rank
        self.random_state = random_state
        self.tol = tol
        self.maxiter = maxiter
        self.rank = rank
       
    def initialize_matrices(self, m, n):
        """Initialize the W and H matrices"""
        #Randomly initialize the W and the H
        np.random.seed(self.random_state)
        W = np.random.random((m,self.rank))
        H = np.random.random((self.rank,n))
        return W, H
        
    def compute_loss(self, V, W, H):
        """Computes the loss of the algorithm according to the frobenius norm"""
        #Computes the loss as a frobenius norm
        return np.linalg.norm(V-W@H, ord="fro")
    
    def update_matrices(self, V, W, H):
        """The multiplicative update step to update W and H"""
        #Iterates to improve W and H
        H1 = H*(W.T@V)/(W.T@W@H)
        W1 = W*(V@H1.T)/(W@H1@H1.T)
        return H1, W1
      
    def fit(self, V):
        """Fits W and H weight matrices according to the multiplicative update 
        algorithm. Return W and H"""
        #Runs the loss and update functions until W and H are stable
        m, n = V.shape
        W, H = self.initialize_matrices(m, n)
        Xprev = 0
        i = 0
        while i < self.maxiter and self.compute_loss(V, W, H) > self.tol and np.linalg.norm(Xprev - W@H, ord='fro')>self.tol:
            #print(self.compute_loss(V, W, H))
            Xprev = W @ H
            H, W = self.update_matrices(V, W, H)
            i += 1
        return W, H

    def reconstruct(self, W, H):
        """Reconstructs the V matrix for comparison against the original V 
        matrix"""
        #Literally just W @ H
        return W @ H


        
def prob4():
    """Run NMF recommender on the grocery store example"""
    V = np.array([[0,1,0,1,2,2],
                  [2,3,1,1,2,2],
                  [1,1,1,0,1,1],
                  [0,2,3,4,1,1],
                  [0,0,0,0,1,0]])
    #Run the algorithm!
    nmf = NMFRecommender(rank=2)
    W, H = nmf.fit(V)
    #count how many fit the criteria
    count = 0
    for X in W:
        if X[0] < X[1]:
            count += 1
    return W, H, count


def prob5():
    """
    Calculate the rank and run NMF
    """
    #Load data
    df = pd.read_csv("artist_user.csv")
    df.set_index('Unnamed: 0', inplace=True)
    
    #Find the tolerance
    Tol = np.linalg.norm(df.values, ord='fro')*.0001
    
    #Start at rank 1
    rank = 1
    model = NMF(n_components=rank, init='random', random_state=0, max_iter=400)
    W = model.fit_transform(df.values)
    H = model.components_
    #Search for a rank that drops below our tolerance!
    while np.sqrt(mse(df.values, W@H)) > Tol:
        rank += 1
        model = NMF(n_components=rank, init='random', random_state=0, max_iter=400)
        W = model.fit_transform(df.values)
        H = model.components_
    return rank, W@H

def discover_weekly(ID, V):
    """
    Create the recommended weekly 30 list for a given user
    """
    #Load Data
    df = pd.read_csv("artist_user.csv")
    df.set_index('Unnamed: 0', inplace=True)
    vector = np.array(df.loc[ID])
    Index = list(df.index).index(2)
    
    #Get user vector
    Vestimation = V[Index]
    for x in np.nonzero(vector):
        Vestimation[x] = 0
    Result = np.array(df.columns)[np.argsort(Vestimation)[::-1]]
    
    #Load Artists
    artists = pd.read_csv("artists.csv")
    artists.set_index('id',inplace=True)
    
    #Build the recommended list
    Recommend = []
    for x in Result:
        Recommend.append(list(artists.loc[int(x)].values))
    return Recommend[0:30]