"""
Random Forest Lab

Nathaniel Neubert
16/11/2020
"""
#Import required packages
import graphviz
import os
from uuid import uuid4
import numpy as np
from sklearn.ensemble import RandomForestClassifier as RFC
import time as t

# Problem 1
class Question:
    """Questions to use in construction and display of Decision Trees.
    Attributes:
        column (int): which column of the data this question asks
        value (int/float): value the question asks about
        feature (str): name of the feature asked about
    Methods:
        match: returns boolean of if a given sample answered T/F"""
    
    def __init__(self, column, value, feature_list):
        #Store the desired variables
        self.column = column
        self.value = value
        self.feature = feature_list[self.column]
    
    def match(self,sample):
        """Returns T/F depending on how the sample answers the question
        Parameters:
            sample ((n,), ndarray): New sample to classify
        Returns:
            (bool): How the sample compares to the question"""
        #Check to see if it fulfills the criteria, return True if yes, False if no
        if sample[self.column] >= self.value: # Check if the sample is greater than the value
            return True
        else:
            return False
        
    def __repr__(self):
        #prints the question 
        return "Is %s >= %s?" % (self.feature, str(self.value))
    
def partition(data,question):
    """Splits the data into left (true) and right (false)
    Parameters:
        data ((m,n), ndarray): data to partition
        question (Question): question to split on
    Returns:
        left ((j,n), ndarray): Portion of the data NOT matching the question
        right ((m-j, n), ndarray): Portion of the data matching the question
        """
    #Initialize both as None
    left = None
    right = None
    #Check each row of the data and assign it a side
    for row in data:
        if question.match(row):
            try:
                left.append(row)
            except:
                left = [row]
        else:
            try:
                right.append(row)
            except:
                right = [row]
    
    #If a region is empty return none
    if left == None:
        return left, np.array(right)
    if right == None:
        return np.array(left), right
    
    #if both have elements, return their arrays
    return np.array(left), np.array(right)
    
#Problem 2    
def gini(data):
    """Return the Gini impurity of given array of data.
    Parameters:
        data (ndarray): data to examine
    Returns:
        (float): Gini impurity of the data"""
    #Find N value    
    N = data.shape[0]
    
    #Find all the unique values and how many times they are counted
    unique, counts = np.unique(data[:,-1], return_counts=True)
    
    #calculate the gini value
    ginminus = 0
    for i in range(len(unique)):
        ginminus += (counts[i]/N)**2
    return 1 - ginminus

def info_gain(left,right,old_info):
    """Return the info gain of a partition of data.
    Parameterss:
        left (ndarray): left split of data
        right (ndarray): right split of data
        old (float): Gini impurity of unsplit data
    Returns:
        (float): info gain of the data"""
    try:
        #Use the gini function to calculate the info gain
        return gini(old_info) - left.shape[0]/old_info.shape[0]*gini(left) - right.shape[0]/old_info.shape[0]*gini(right)
    except: 
        print(left)
        print(right)
        print(old_info)
    
# Problem 3, Problem 7
def find_best_split(data, feature_labels, min_samples_leaf=5, random_subset=False):
    """Find the optimal split
    Parameters:
        data (ndarray): Data in question
        feature_labels (list of strings): Labels for each column of data
        min_samples_leaf (int): minimum number of samples per leaf
        random_subset (bool): for Problem 7
    Returns:
        (float): Best info gain
        (Question): Best question
    """
    BIG = -1 #Biggest info gain
    BQ = 0 #Best question
    cols = list(range(data.shape[1]-1))
    
    #Select a subset of the features if random is on
    if random_subset:
        sqrn = np.floor(np.sqrt(len(cols)))
        while len(cols) > sqrn:
            rem = np.random.randint(0,len(cols)-1)
            cols.pop(rem)
            
    #Search for the best possible question
    for col in cols:
        unique = np.unique(data[:,col])
        for val in unique:
            q = Question(int(col),val,feature_labels)
            left, right = partition(data, q)
            #print(type(left))
            #print(type(right))
            if type(left) != type(None) and type(right) != type(None):
                IG = info_gain(left, right, data)
                if IG > BIG and len(left) >= min_samples_leaf and len(right) >= min_samples_leaf:
                    BIG = IG
                    BQ = q
    
    #return the best info gain and the best question
    return BIG, BQ

# Problem 4
class Leaf:
    """Tree leaf node
    Attribute:
        prediction (dict): Dictionary of labels at the leaf"""
    def __init__(self,data):
        #Store useful info
        self.data = data
        unique, counts = np.unique(data[:,-1], return_counts=True)
        self.prediction = {}
        for i in range(len(unique)):
            self.prediction[int(unique[i])] = counts[i] #Dictionary of trues and falses

class Decision_Node:
    """Tree node with a question
    Attributes:
        question (Question): Question associated with node
        left (Decision_Node or Leaf): child branch
        right (Decision_Node or Leaf): child branch"""
    def __init__(self, question, right_branch, left_branch):
        #Store useful info
        self.question = question
        self.left = left_branch
        self.right = right_branch

## Code to draw a tree
def draw_node(graph, my_tree):
    """Helper function for drawTree"""
    node_id = uuid4().hex
    #If it's a leaf, draw an oval and label with the prediction
    if isinstance(my_tree, Leaf):
        graph.node(node_id, shape="oval", label="%s" % my_tree.prediction)
        return node_id
    else: #If it's not a leaf, make a question box
        graph.node(node_id, shape="box", label="%s" % my_tree.question)
        left_id = draw_node(graph, my_tree.left)
        graph.edge(node_id, left_id, label="T")
        right_id = draw_node(graph, my_tree.right)    
        graph.edge(node_id, right_id, label="F")
        return node_id

def draw_tree(my_tree):
    """Draws a tree"""
    #Remove the files if they already exist
    for file in ['Digraph.gv','Digraph.gv.pdf']:
        if os.path.exists(file):
            os.remove(file)
    graph = graphviz.Digraph(comment="Decision Tree")
    draw_node(graph, my_tree)
    graph.render(view=True) #This saves Digraph.gv and Digraph.gv.pdf

# Prolem 5
def build_tree(data, feature_names, min_samples_leaf=5, max_depth=4, current_depth=0, random_subset=False):
    """Build a classification tree using the classes Decision_Node and Leaf
    Parameters:
        data (ndarray)
        feature_names(list or array)
        min_samples_leaf (int): minimum allowed number of samples per leaf
        max_depth (int): maximum allowed depth
        current_depth (int): depth counter
        random_subset (bool): whether or not to train on a random subset of features
    Returns:
        Decision_Node (or Leaf)"""
    i = current_depth #store the current depth as i
    #Find the initial best split
    IG, Q = find_best_split(data,  feature_names, min_samples_leaf, random_subset=random_subset) #find the best split
    
    #If we meet our stopping criteria then quit!
    if i == max_depth or IG == 0 or type(Q) == type(0): #if we meet the breaking crit, get out!
        return Leaf(data)
    
    #Partition the data otherwise
    left, right = partition(data, Q) 
    
    #Check if any leaf will be too small
    if len(left) >= min_samples_leaf and len(right) >= min_samples_leaf: # Both leaves are large enough
        return Decision_Node(Q, build_tree(right, feature_names, min_samples_leaf, max_depth, i+1, random_subset=random_subset), build_tree(left, feature_names, min_samples_leaf, max_depth, i+1, random_subset=random_subset))
    elif len(left) < min_samples_leaf or len(right) < min_samples_leaf: #Either leaf is too small
        return Leaf(data)
    #If we get here... we've done something wrong    
    return Failure

# Problem 6
def predict_tree(sample, my_tree):
    """Predict the label for a sample given a pre-made decision tree
    Parameters:
        sample (ndarray): a single sample
        my_tree (Decision_Node or Leaf): a decision tree
    Returns:
        Label to be assigned to new sample"""
    try: #function rerun in case of a decision node
        if my_tree.question.match(sample):
            return predict_tree(sample, my_tree.left)
        else:
            return predict_tree(sample, my_tree.right)
    except: #returns the top class if it is a leaf
        return max(my_tree.prediction, key=my_tree.prediction.get)
    
def analyze_tree(dataset,my_tree):
    """Test how accurately a tree classifies a dataset
    Parameters:
        dataset (ndarray): Labeled data with the labels in the last column
        tree (Decision_Node or Leaf): a decision tree
    Returns:
        (float): Proportion of dataset classified correctly"""
    correct = 0 #initialize the correct counter
    for x in range(len(dataset)): #Check each of the vars to see if they can be assertained.
        if predict_tree(dataset[x], my_tree) == dataset[x,-1]:
            correct += 1
    return (correct/len(dataset))

# Problem 7
def predict_forest(sample, forest):
    """Predict the label for a new sample, given a random forest
    Parameters:
        sample (ndarray): a single sample
        forest (list): a list of decision trees
    Returns:
        Label to be assigned to new sample"""
    lenforest = len(forest) #Get the overall voting roster
    upvote = 0
    for tree in forest: #let each tree in the forest vote!
        upvote += predict_tree(sample, tree)
    if upvote/len(forest) >= .5:
        return 1
    else:
        return 0

def analyze_forest(dataset,forest):
    """Test how accurately a forest classifies a dataset
    Parameters:
        dataset (ndarray): Labeled data with the labels in the last column
        forest (list): list of decision trees
    Returns:
        (float): Proportion of dataset classified correctly"""
    correct = 0
    for x in range(len(dataset)): #check each sample in the dataset on the forest
        #print(predict_forest(dataset[x], forest))
        #print("data" +str(dataset[x,-1]))
        if predict_forest(dataset[x], forest) == dataset[x,-1]:
            correct += 1
    return correct/len(dataset)

# Problem 8
def prob8():
    """Using the file parkinsons.csv, return six items:
        Your accuracy in a 5-tree forest
        The time it took to run your 5-tree forest
        Scikit-Learn's accuracy in a 5-tree forest
        The time it took to run that 5-tree forest
        Scikit-Learn's accuracy in a forest with default parameters
        The time it took to run that forest with default parameters
    
    Returns:
        Three tuples each containing the accuracy then the time taken.
    """
    #Read in data
    Xfull = np.loadtxt('parkinsons.csv', delimiter = ',')
    features = np.loadtxt('parkinsons_features.csv', delimiter=',', dtype=str, comments=None)
    
    #Access Random training and test sets
    trainRows = np.random.randint(0,Xfull.shape[0],100)
    testRows = np.random.randint(0,Xfull.shape[0],30)
    X = Xfull[trainRows]
    Xtest = Xfull[testRows]
    
    #My 5 tree Forest
    start = t.time()
    myforest = []
    for i in range(0,5):
        mytree = build_tree(X, features, 15, random_subset=True)
        myforest.append(mytree)
    myscore = analyze_forest(Xtest, myforest)
    mytime = t.time()-start
    
    
    #Sklearn 5 tree forest
    start = t.time()
    forest = RFC(n_estimators=5, min_samples_leaf=15)
    forest.fit(X[:,:-1], X[:,-1])
    skscore = forest.score(Xtest[:,:-1], Xtest[:,-1])
    sktime = t.time()-start
    
    #Sklearn default
    start = t.time()
    standforest = RFC()
    standforest.fit(X[:,:-1], X[:,-1])
    standskscore = standforest.score(Xtest[:,:-1], Xtest[:,-1])
    standsktime = t.time()-start
    
    return (myscore, mytime), (skscore, sktime), (standskscore, standsktime)